#!/usr/bin/env bash

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2018, Day 1 - Chronal Calibration
#
# https://adventofcode.com/2018/day/1

./day1.rb < day1.in
