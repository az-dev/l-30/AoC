#!/usr/bin/env ruby

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2018, Day 1 - Chronal Calibration
#
# https://adventofcode.com/2018/day/1


DEBUG = true

def main()
  # frequency starts at zero
  frequency = 0

  # frequencies read by the device
  frequency_changes = []

  # resulting frequencies AFTER applying the frequency read
  resulting_frequency = [0]

  # read the puzzle input and process it
  $stdin.each do |line|
    f = line.to_i
    frequency += f

    if DEBUG
      puts "frequency: #{frequency}\tchange: #{f}"
      puts "changes: #{frequency_changes}"
      puts "resulting: #{resulting_frequency}"
      80.times do print "=" end
      puts ""
    end

    if resulting_frequency.include? frequency
      puts frequency
      exit
    end

    frequency_changes << f            # save in case we need to iterate again
    resulting_frequency << frequency  # to test if we already saw that frequency
  end

  # loop do
  #   frequency_changes.each do |f|
  #     frequency += f

  #     if resulting_frequency.include? frequency
  #       puts frequency
  #       exit
  #     end

  #     resulting_frequency << frequency  # to test if we already saw that frequency
  #   end
  # end
end

# entry point, _à la python_
main()
