#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Advent of Code 2015, Day 3 - Perfectly Spherical Houses in a Vacuum

https://adventofcode.com/2015/day/3
"""


import sys


DEBUG = False


def move(c, x, y):
    if c == '^':
        y += 1
    elif c == 'v':
        y -= 1
    elif c == '>':
        x += 1
    elif c == '<':
        x -= 1
    else:
        if DEBUG:
            print("unknown {}".format(c))

    return x, y


def day3(line):
    x_santa, y_santa = 0, 0
    x_robo_santa, y_robo_santa = 0, 0
    x, y = 0, 0
    counter = 0

    houses = {"({}, {})".format(x, y)}

    for c in line:
        if counter % 2 == 0:
            if DEBUG:
                print("Robo-Santa's turn")

            x, y = move(c, x_robo_santa, y_robo_santa)
            x_robo_santa, y_robo_santa = x, y
        else:
            if DEBUG:
                print("Santa's turn")

            x, y = move(c, x_santa, y_santa)
            x_santa, y_santa = x, y

        houses.add("({}, {})".format(x, y))
        counter += 1

    return len(houses)


def main():
    line = sys.stdin.readline().strip()

    print("{}".format(day3(line)))


if __name__ == "__main__":
    main()
