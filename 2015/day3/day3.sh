#!/usr/bin/env bash

# Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2015, Day 3 - Perfectly Spherical Houses in a Vacuum
#
# https://adventofcode.com/2015/day/3

./day3.py < day3.in
