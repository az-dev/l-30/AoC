#!/usr/bin/env bash

# Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2015, Day 4 - The Ideal Stocking Stuffer
#
# https://adventofcode.com/2015/day/4

./day4.py < day4.in
