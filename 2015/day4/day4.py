#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Advent of Code 2015, Day 4 - The Ideal Stocking Stuffer

https://adventofcode.com/2015/day/4
"""


import sys
import itertools
import hashlib
import re


DEBUG = False


def day4(line):
    for i in itertools.count(start=1):
        md5 = hashlib.md5()
        md5.update("{}{}".format(line, i))
        hash = md5.hexdigest()

        if DEBUG:
            print("{}|{} => {}".format(line, i, hash))

        if re.match('^000000', hash):
            return i


def main():
    line = sys.stdin.readline().strip()

    print("{}".format(day4(line)))


if __name__ == "__main__":
    main()
