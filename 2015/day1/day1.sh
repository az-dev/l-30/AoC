#!/usr/bin/env bash

# Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2015, Day 1 - Not Quite Lisp
#
# https://adventofcode.com/2015/day/1

./day1.py < day1.in
