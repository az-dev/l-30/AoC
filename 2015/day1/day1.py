#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Advent of Code 2015, Day 1 - Not Quite Lisp

https://adventofcode.com/2015/day/1
"""


import sys


DEBUG = False


def day1(line):
    open_paren = 0
    closed_paren = 0
    counter = 0

    for c in line:
        counter += 1

        if c == '(':
            open_paren += 1
            if DEBUG:
                print("UP")
        elif c == ')':
            closed_paren -= 1
            if DEBUG:
                print("DOWN")
        else:
            if DEBUG:
                print("unknown {}".format(c))

        if open_paren + closed_paren < 0:
            return counter

    if DEBUG:
        print("open => {}, closed => {}".format(open_paren, closed_paren))

    return open_paren + closed_paren


def main():
    line = sys.stdin.readline()

    print("basement reached at position {}".format(day1(line)))


if __name__ == "__main__":
    main()
