#!/usr/bin/env bash

# Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2015, Day 5 - Doesn't He Have Intern-Elves For This?
#
# https://adventofcode.com/2015/day/5

./day5.py < day5.in
