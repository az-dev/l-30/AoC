#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Advent of Code 2015, Day 5 - Doesn't He Have Intern-Elves For This?

https://adventofcode.com/2015/day/5
"""


import sys
import re


DEBUG = False


def day5(line):
    is_nice = False
    vowel = 0
    letters_in_row = 0
    last_letter = None

    if re.search('ab|cd|pq|xy', line) is not None:
        is_nice = False
        if DEBUG:
            print("\tforbidden sequence")
    else:
        for c in line:
            if c in ['a', 'e', 'i', 'o', 'u']:
                vowel += 1
                if DEBUG:
                    print("\tvowel {}".format(c))

            if last_letter == c:
                letters_in_row += 1
                if DEBUG:
                    print("\tletter in a row {} {}".format(last_letter, c))

            last_letter = c

            if vowel >= 3 and letters_in_row >= 1:
                is_nice = True
                break

    if DEBUG:
        print("{} => {} - {} vowels, {} letters in row".format(is_nice, line, vowel, letters_in_row))

    return 1 if is_nice else 0


def first_rule(line):
    return True


def second_rule(line):
    return True


def day5_part2(line):
    _first_rule = first_rule(line)
    _second_rule = second_rule(line)

    is_nice = _first_rule and _second_rule

    return 1 if is_nice else 0


def main():
    nice = 0

    while True:
        line = sys.stdin.readline().strip()

        if line == "\n" or line == "":
            break

        nice += day5_part2(line)

    print("{}".format(nice))


if __name__ == "__main__":
    main()
