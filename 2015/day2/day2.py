#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
License MIT

Advent of Code 2015, Day 2 - I Was Told There Would Be No Math

https://adventofcode.com/2015/day/2
"""


import sys


DEBUG = False


def day2(line):
    l, w, h = list(map(lambda n: int(n), line.split('x')))
    min = 0

    lw = l * w
    wh = w * h
    hl = h * l
    area = 2 * lw + 2 * wh + 2 * hl

    if lw <= wh and lw <= hl:
        min = lw
    elif wh <= hl and wh <= lw:
        min = wh
    elif hl <= lw and hl <= wh:
        min = hl
    else:
        if DEBUG:
            print("unknown => lw = {} wh = {} hl = {}".format(lw, wh, hl))

    if DEBUG:
        print("l = {} w = {} h = {}".format(l, w, h))
        print("lw = {} wh = {} hl = {}".format(lw, wh, hl))
        print("area = {}, min = {}, total = {}\n".format(area, min, area + min))

    return area + min


def day2_part2(line):
    l, w, h = list(map(lambda n: int(n), line.split('x')))
    volume = l * w * h
    smallest_perimeter = 0

    perimeter_a = 2*l + 2*w
    perimeter_b = 2*w + 2*h
    perimeter_c = 2*h + 2*l

    if perimeter_a <= perimeter_b and perimeter_a <= perimeter_c:
        smallest_perimeter = perimeter_a
    elif perimeter_b <= perimeter_c and perimeter_b <= perimeter_a:
        smallest_perimeter = perimeter_b
    elif perimeter_c <= perimeter_a and perimeter_c <= perimeter_b:
        smallest_perimeter = perimeter_c
    else:
        if DEBUG:
            print("unknown => lw = {} wh = {} hl = {}".format(perimeter_a, perimeter_b, perimeter_c))

    return volume + smallest_perimeter


def main():
    ribbon = 0
    wrapping_paper = 0

    while True:
        line = sys.stdin.readline()

        if line == "\n" or line == "":
            break

        ribbon += day2_part2(line)
        wrapping_paper += day2(line)

    print("{}".format(ribbon))


if __name__ == "__main__":
    main()
