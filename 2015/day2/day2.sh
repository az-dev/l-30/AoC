#!/usr/bin/env bash

# Copyright (c) 2019 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2015, Day 2 - I Was Told There Would Be No Math
#
# https://adventofcode.com/2015/day/2

./day2.py < day2.in
