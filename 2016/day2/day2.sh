#!/usr/bin/env bash

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2016, Day 2 - Bathroom Security
#
# https://adventofcode.com/2016/day/2

cargo run  < day2.in
