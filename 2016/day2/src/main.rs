// Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
// License MIT
//
// Advent of Code 2016, Day 2 - Bathroom Security
//
// https://adventofcode.com/2016/day/2

use std::io::{self, BufRead};

static DEBUG: bool= false;

fn main() {
    let mut code: Vec<i32> = Vec::new();

    for line in io::stdin().lock().lines() {
        let mut pos: i32 = match code.last() {
            Some(n) => n.clone(),
            None => 5
        };

        match line {
            Ok(string) => {
                for c in string.chars() {
                    pos = match c {
                        'U' => {
                            match pos {
                                5 => 5,
                                2 => 2,
                                6 => 2,
                                10 => 6,
                                1 => 1,
                                3 => 1,
                                7 => 3,
                                11 => 7,
                                13 => 11,
                                4 => 4,
                                8 => 4,
                                12 => 8,
                                9 => 9,
                                _ => 0
                            }
                        },
                        'D' => {
                            match pos {
                                5 => 5,
                                2 => 6,
                                6 => 10,
                                10 => 10,
                                1 => 3,
                                3 => 7,
                                7 => 11,
                                11 => 13,
                                13 => 13,
                                4 => 8,
                                8 => 12,
                                12 => 12,
                                9 => 9,
                                _ => 0
                            }
                        },
                        'L' => {
                            if pos == 1 { pos }
                            else if (pos >= 2 && pos <= 4) && ((pos - 1) >= 2 && (pos - 1) <= 4) { pos - 1 }
                            else if (pos >= 5 && pos <= 9) && ((pos - 1) >= 5 && (pos - 1) <= 9) { pos - 1 }
                            else if (pos >= 10 && pos <= 12) && ((pos - 1) >= 10 && (pos - 1) <= 12) { pos - 1 }
                            else if pos == 13 { pos }
                            else { pos }
                        },
                        'R' => {
                            if pos == 1 { pos }
                            else if (pos >= 2 && pos <= 4) && ((pos + 1) >= 2 && (pos + 1) <= 4) { pos + 1 }
                            else if (pos >= 5 && pos <= 9) && ((pos + 1) >= 5 && (pos + 1) <= 9) { pos + 1 }
                            else if (pos >= 10 && pos <= 12) && ((pos + 1) >= 10 && (pos + 1) <= 12) { pos + 1 }
                            else if pos == 13 { pos }
                            else { pos }
                        },
                        _ => { 0 }
                    };
                    if DEBUG { print!("{:?}", pos); }
                }
                if DEBUG { println!(""); }
                code.push(pos);
            },
            Err(_) => break
        }
    }

    for n in code.iter() {
        print!("{}",
            match n {
                10 => { "A".to_string() },
                11 => { "B".to_string() },
                12 => { "C".to_string() },
                13 => { "D".to_string() },
                _ => n.to_string()
        });
    }
    println!("");
}
