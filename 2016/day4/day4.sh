#!/usr/bin/env bash

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2016, Day 4 - Security Through Obscurity
#
# https://adventofcode.com/2016/day/4

cargo run  < day4.in
