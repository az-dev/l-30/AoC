// Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
// License MIT
//
// Advent of Code 2016, Day 4 - Security Through Obscurity
//
// https://adventofcode.com/2016/day/4

use std::io::{self, BufRead};
use std::collections::HashMap;
use std::cmp::Ordering;

static DEBUG: bool = false;

#[derive(Debug)]
struct Room {
    name: String,
    id: i32,
    checksum: String,
}

impl Room {
    fn new(string: String) -> Room {
        let id: i32;
        let checksum: String;
        let name: String;

        let mut tokens: Vec<&str>;

        tokens = string.split('[').collect();
        checksum = tokens.pop().unwrap().trim_matches(']').to_string();

        tokens = tokens.pop().unwrap().split('-').collect();
        id = tokens.pop().unwrap().parse().unwrap();

        name = tokens.join("-");

        if DEBUG { println!("{:?}, {:?}, {:?}", id, checksum, name); }

        Room {
            id: id,
            checksum: checksum,
            name: name,
        }
    }

    fn is_real(&self) -> bool {
        self.do_checksum() == self.checksum
    }

    fn do_checksum(&self) -> String {
        let mut dic: HashMap<char, i32> = HashMap::new();
        for s in self.name.chars() {
            if s == '-' { continue }

            let count = dic.entry(s).or_insert(0);
            *count += 1;
        }

        let mut chars: Vec<(&char, &i32)> = dic.iter().collect();
        chars.sort_by(|a, b| {
            match a.1.cmp(b.1) {
                Ordering::Greater => Ordering::Greater,
                Ordering::Less => Ordering::Less,
                Ordering::Equal => {
                    match b.0.cmp(a.0) {
                        Ordering::Greater => Ordering::Greater,
                        Ordering::Less => Ordering::Less,
                        Ordering::Equal => Ordering::Equal,
                    }
                }
            }
        });

        let mut ans = String::new();
        for _i in 0..5 {
            ans.push(chars.pop().unwrap().0.clone());
        }

        ans
    }

    fn decrypt(&self) -> String {
        let mut x = self.name.chars();
        for _i in 0..self.id {
            for i in x {
                //if i == '-' { continue }

            }
        }

        "".to_string()
    }
}

fn main() {
    let mut rooms: Vec<Room> = Vec::new();
    let mut sum_id: i32 = 0;

    for line in io::stdin().lock().lines() {
        match line {
            Ok(string) => {
                if DEBUG { println!("{:?}", string); }
                rooms.push(Room::new(string));
            },
            Err(_) => break
        }
    }

    for r in rooms.iter() {
        if r.is_real() {
            sum_id += r.id;
            r.decrypt();
        }
    }

    println!("{:?}", sum_id);
}
