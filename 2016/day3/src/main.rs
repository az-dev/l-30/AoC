// Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
// License MIT
//
// Advent of Code 2016, Day 3 - Squares With Three Sides
//
// https://adventofcode.com/2016/day/3

use std::io::{self, BufRead};

static DEBUG: bool= false;

fn main() {
    let mut valid_triangles: i32 = 0;

    let mut a: Vec<i32> = Vec::new();
    let mut b: Vec<i32> = Vec::new();
    let mut c: Vec<i32> = Vec::new();

    for line in io::stdin().lock().lines() {
        match line {
            Ok(string) => {
                let mut tmp: Vec<&str> = string.trim().split_whitespace().collect();

                c.push(match tmp.pop() {
                    Some(string) => string.parse().unwrap(),
                    None => 0
                });
                b.push(match tmp.pop() {
                    Some(string) => string.parse().unwrap(),
                    None => 0
                });
                a.push(match tmp.pop() {
                    Some(string) => string.parse().unwrap(),
                    None => 0
                });
            },
            Err(_) => break
        }
    }

    a.append(&mut b);
    a.append(&mut c);
    a.reverse();

    let mut p = 0;
    let max = a.len();
    while p < max {
        p += 3;

        let mut valid = false;

        let i = a.pop().unwrap();
        let j = a.pop().unwrap();
        let k = a.pop().unwrap();

        if i + j > k && i + k > j && k + j > i {
            valid = true;
            valid_triangles += 1;
        }

        if DEBUG { println!("{}, {}, {}, {}", i, j, k, valid ); }
    }

    println!("{:?}", valid_triangles);
}
