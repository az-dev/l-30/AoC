#!/usr/bin/env bash

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2016, Day 3 - Squares With Three Sides
#
# https://adventofcode.com/2016/day/3

cargo run  < day3.in
