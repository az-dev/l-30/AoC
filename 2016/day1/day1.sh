#!/usr/bin/env bash

# Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
# License MIT
#
# Advent of Code 2016, Day 1 - No Time for a Taxicab
#
# https://adventofcode.com/2016/day/1

cargo run  < day1.in

# To generate the python program, set to true the python variable in src/main.rs
# cargo run  < day1.in > day1.py
