// Copyright (c) 2018 Manuel Alejandro Gómez Nicasio <az-dev@outlook.com>
// License MIT
//
// Advent of Code 2016, Day 1 - No Time for a Taxicab
//
// https://adventofcode.com/2016/day/1

use std::io;

fn main() {
    let debug_part1 = false;
    let debug_part2 = false;
    let python = false;

    let mut facing = "north";
    let mut x: i32 = 0;
    let mut y: i32 = 0;

    let mut input = String::new();
    let _direction: String;
    let _length: String;

    let mut found = false;
    let mut increment_over: char = '0';
    let mut points: Vec<(i32, i32)> = Vec::new();
    points.push((0, 0));

    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");

    if debug_part1 { println!("Facing, Moving, Length, Point"); }

    if python {
        println!("#!/usr/bin/env python");
        println!("# -*- coding: utf-8 -*-");
        println!("");
        println!("import turtle");
        println!("");
    }

    for mov in input.split(',') {
        let (_direction, _length) = mov.trim().split_at(1);
        let mut _facing = "";

        let _direction = _direction.to_uppercase();
        let _length: i32 = _length.parse().unwrap();

        if facing == "north" && _direction == "L" || facing == "south" && _direction == "R" {
            x -= _length;
            _facing = "west";
            increment_over = 'x';
        } else if facing == "north" && _direction == "R" || facing == "south" && _direction == "L" {
            x += _length;
            _facing = "east";
            increment_over = 'x';
        } else if facing == "east" && _direction == "L" || facing == "west" && _direction == "R" {
            y += _length;
            _facing = "north";
            increment_over = 'y';
        } else if facing == "east" && _direction == "R" || facing == "west" && _direction == "L" {
            y -= _length;
            _facing = "south";
            increment_over = 'y';
        }

        //
        let (x0, y0) = points.pop().unwrap().clone();
        if debug_part2 {
            println!("from ({}, {}) to ({}, {})", x0, y0, x, y);
        }

        if increment_over == 'x' {
            if debug_part2 { println!("x"); }

            if x0 < x {
                let mut i = x0;
                while i < x {
                    if points.contains(&(i, y0)) {
                        found = true;
                        x = i;
                        y = y0;
                        break;
                    }

                    points.push((i, y0));
                    if debug_part2 { println!("({}, {})", i, y0); }
                    i += 1;
                }
            } else {
                let mut i = x0;
                while i > x {
                    if points.contains(&(i, y0)){
                        found = true;
                        x = i;
                        y = y0;
                        break;
                    }

                    points.push((i, y0));
                    if debug_part2 { println!("({}, {})", i, y0); }
                    i -= 1;
                }
            }
        } else {
            if debug_part2 { println!("y"); }

            if y0 < y {
                let mut i = y0;
                while i < y {
                    if points.contains(&(x0, i)) {
                        found = true;
                        x = x0;
                        y = i;
                        break;
                    }

                    points.push((x0, i));
                    if debug_part2 { println!("({}, {})", x0, i); }
                    i += 1;
                }
            } else {
                let mut i = y0;
                while i > y {
                    if points.contains(&(x0, i)) {
                        found = true;
                        x = x0;
                        y = i;
                        break;
                    }

                    points.push((x0, i));
                    if debug_part2 { println!("({}, {})", x0, i); }
                    i -= 1;
                }
            }

        }
        //

        points.push((x, y));

        if python {
            println!("turtle.{}(90)", if _direction == "L" { "left" } else { "right" });
            println!("turtle.forward({})", _length);
            println!("");
        }

        if debug_part1 {
            println!("{}, {}, {}, ({},{})", facing, _direction, _length, x, y);
        }
        facing = _facing;

        if found {
            break;
        }
    }

    if python {
        println!("x, y = turtle.position()");
        println!("print(x + y)");
    } else {
        println!("{}", (x.abs() + y.abs()));

        if debug_part2 {
            for (a, b) in points.iter() {
                println!("({}, {})", a, b);
            }
        }
    }
}
